import { Stats, MetaOutput, FighterBuild, createFighterBuild, BuildType, BuildResult } from '.';
import { isStats } from './typeUtils';

/**
 * Compute the item's stats MetaScore.
 *
 * @param stats
 * @returns MetaScore
 */
export function computeStatsBuildMetaScore(stats: Stats): BuildResult {
  if (!isStats(stats)) throw new Error('Malformed or invalid item stats.');
  return simulatorResults(stats);
}

export function simulatorResults(stats: Stats): BuildResult {
  const barbarianBuild =
    Math.max(stats.agility * 2 + stats.strength * 1.4, stats.agility * 1.4 + stats.strength * 2) +
    stats.intelligence * 0.1 +
    stats.charm * 0.5 +
    stats.luck * 1 +
    stats.wisdom * 1;
  const druidBuild =
    Math.max(stats.agility * 2 + stats.intelligence * 1.4, stats.agility * 1.4 + stats.intelligence * 2) +
    stats.strength * 0.1 +
    stats.charm * 1 +
    stats.luck * 0.5 +
    stats.wisdom * 1;
  const paladinBuild =
    Math.max(stats.intelligence * 2 + stats.strength * 1.4, stats.intelligence * 1.4 + stats.strength * 2) +
    stats.agility * 0.1 +
    stats.charm * 1 +
    stats.luck * 1 +
    stats.wisdom * 0.5;
  const rogueBuild = stats.agility * 2.75 + stats.wisdom + stats.charm * 0.5 + stats.luck * 0.5;
  const warriorBuild = stats.strength * 2.75 + stats.luck + stats.charm * 0.5 + stats.wisdom * 0.5;
  const wizardBuild = stats.intelligence * 2.75 + stats.charm + stats.luck * 0.5 + stats.wisdom * 0.5;

  const metaScore = Math.max(...[barbarianBuild, druidBuild, paladinBuild, rogueBuild, warriorBuild, wizardBuild]);

  return {
    metaScore,
    currentClass: ['none', 'barbarian', 'druid', 'paladin', 'rogue', 'warrior', 'wizard'][
      [0, barbarianBuild, druidBuild, paladinBuild, rogueBuild, warriorBuild, wizardBuild].indexOf(metaScore)
    ] as BuildType,
    classes: {
      none: metaScore,
      warrior: warriorBuild,
      paladin: paladinBuild,
      barbarian: barbarianBuild,
      wizard: wizardBuild,
      rogue: rogueBuild,
      druid: druidBuild,
    },
  };
}

export function getFighterBuildMetaOutputs(build: FighterBuild): MetaOutput[] {
  let metas: MetaOutput[] = [];
  const baseBuild: FighterBuild = createFighterBuild();
  metas.push(getMetaScoreOutput(build.maxHP - baseBuild.maxHP, 270));
  metas.push(getMetaScoreOutput(build.minDamage + build.maxDamage - (baseBuild.minDamage + baseBuild.maxDamage), 80));
  metas.push(getMetaScoreOutput(build.hitChance - baseBuild.hitChance, 39));
  metas.push(getMetaScoreOutput(build.hitFirst - baseBuild.hitFirst, 25));
  metas.push(getMetaScoreOutput(build.critDamageMultiplier - baseBuild.critDamageMultiplier, 1));
  metas.push(getMetaScoreOutput(build.meleeCrit - baseBuild.meleeCrit, 35));
  metas.push(getMetaScoreOutput(build.critResist - baseBuild.critResist, 0.75));
  metas.push(getMetaScoreOutput(build.evadeChance - baseBuild.evadeChance, 43));
  metas.push(getMetaScoreOutput(build.meleeResist - baseBuild.meleeResist, 23));
  return metas;
}

export function getFighterBuildMetaOutputsLarge(build: FighterBuild): MetaOutput[] {
  let metas: MetaOutput[] = [];
  const baseBuild: FighterBuild = createFighterBuild();
  metas.push(getMetaScoreOutputLarge(build.maxHP - baseBuild.maxHP, 270));
  metas.push(
    getMetaScoreOutputLarge(build.minDamage + build.maxDamage - (baseBuild.minDamage + baseBuild.maxDamage), 80)
  );
  metas.push(getMetaScoreOutputLarge(build.hitChance - baseBuild.hitChance, 39));
  metas.push(getMetaScoreOutputLarge(build.hitFirst - baseBuild.hitFirst, 25));
  metas.push(getMetaScoreOutputLarge(build.critDamageMultiplier - baseBuild.critDamageMultiplier, 1));
  metas.push(getMetaScoreOutputLarge(build.meleeCrit - baseBuild.meleeCrit, 35));
  metas.push(getMetaScoreOutputLarge(build.critResist - baseBuild.critResist, 0.75));
  metas.push(getMetaScoreOutputLarge(build.evadeChance - baseBuild.evadeChance, 43));
  metas.push(getMetaScoreOutputLarge(build.meleeResist - baseBuild.meleeResist, 23));
  return metas;
}

export function getMetaScoreOutput(metaScore: number | undefined, maxMeta: number): MetaOutput {
  if ((metaScore || 0) > maxMeta * (5 / 6)) return 'Meta';
  if ((metaScore || 0) > maxMeta * (4 / 6)) return 'Good';
  if ((metaScore || 0) > maxMeta * (3 / 6)) return 'Decent';
  if ((metaScore || 0) > maxMeta * (2 / 6)) return 'Average';
  if ((metaScore || 0) > maxMeta * (1 / 6)) return 'Moderate';
  else return 'Inferior';
}

export function getMetaScoreOutputLarge(metaScore: number | undefined, maxMeta: number): MetaOutput {
  if ((metaScore || 0) > maxMeta * (7 / 8)) return 'OP';
  if ((metaScore || 0) > maxMeta * (6 / 8)) return 'Meta';
  if ((metaScore || 0) > maxMeta * (5 / 8)) return 'Good';
  if ((metaScore || 0) > maxMeta * (4 / 8)) return 'Decent';
  if ((metaScore || 0) > maxMeta * (3 / 8)) return 'Average';
  if ((metaScore || 0) > maxMeta * (2 / 8)) return 'Moderate';
  if ((metaScore || 0) > maxMeta * (1 / 8)) return 'Inferior';
  if ((metaScore || 0) > 0) return 'Trash';
  return 'Ruygye';
}

export function getLevelMeta(level: number): MetaOutput {
  return getMetaScoreOutput((level / 8) * 100, 100);
}

export function getGenMeta(gen: number): MetaOutput {
  switch (gen) {
    case 1:
      return getMetaScoreOutput(100, 100);
    case 2:
      return getMetaScoreOutput(75, 100);
    case 3:
      return getMetaScoreOutput(55, 100);
    default:
      return getMetaScoreOutput(0, 100);
  }
}
