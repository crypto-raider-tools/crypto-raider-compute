import { FighterBuild, FighterStats, FighterStatsEssentials, Item, KnickknackName, Stats } from '.';

export const isFighterStats = (data: any): data is FighterStats => {
  if ('object' !== typeof data) return false;
  if ('object' !== typeof data?.stats || !isStats(data.stats)) return false;
  if ('object' === typeof data?.mainHand && !isItem(data.mainHand)) return false;
  if ('object' === typeof data?.dress && !isItem(data.dress)) return false;
  if ('object' === typeof data?.finger && !isItem(data.finger)) return false;
  if ('object' === typeof data?.neck && !isItem(data.neck)) return false;
  if ('object' === typeof data?.knickknack && !isItem(data.knickknack) && !isKnickknackName(data.knickknack.name))
    return false;
  return true;
};

export const isFighterStatsEssentials = (data: any): data is FighterStatsEssentials => {
  if ('object' !== typeof data) return false;
  if ('object' !== typeof data?.stats || !isStats(data.stats)) return false;
  if ('object' === typeof data?.mainHand && !isStats(data.mainHand)) return false;
  if ('object' === typeof data?.dress && !isStats(data.dress)) return false;
  if ('object' === typeof data?.finger && !isStats(data.finger)) return false;
  if ('object' === typeof data?.neck && !isStats(data.neck)) return false;
  if ('object' === typeof data?.knickknack && !isKnickknackName(data.knickknack?.name)) return false;
  return true;
};

export const isKnickknackName = (name: any): name is KnickknackName => {
  const knickknacksNames: Array<KnickknackName> = [
    'Bash',
    'Blind',
    'Escape Artist',
    'Ice Barrier',
    'Stone Skin',
    'Enflame',
    'Thorns',
  ];
  return knickknacksNames.indexOf(name) !== -1;
};

export const isItem = (data: any): data is Item => {
  if ('object' !== typeof data) return false;
  if ('string' !== typeof data?.rarity) return false;
  if ('string' !== typeof data?.slot) return false;
  if ('object' !== typeof data?.stats || !isStats(data.stats)) return false;
  return true;
};

export const isStats = (data: any): data is Stats => {
  if ('object' !== typeof data) return false;
  if ('number' !== typeof data?.strength) return false;
  if ('number' !== typeof data?.intelligence) return false;
  if ('number' !== typeof data?.agility) return false;
  if ('number' !== typeof data?.wisdom) return false;
  if ('number' !== typeof data?.charm) return false;
  if ('number' !== typeof data?.luck) return false;
  return true;
};

export function createFighterBuild(): FighterBuild {
  return {
    maxHP: 100,
    minDamage: 4,
    maxDamage: 6,
    hitChance: 90,
    hitFirst: 50,
    critDamageMultiplier: 1.35,
    meleeCrit: 10,
    critResist: 0,
    evadeChance: 10,
    meleeResist: 2,
  };
}

export function fixStats(data: any): Stats {
  if (!data) return createStats();
  return {
    strength: data.hasOwnProperty('strength') ? data.strength : 0,
    intelligence: data.hasOwnProperty('intelligence') ? data.intelligence : 0,
    agility: data.hasOwnProperty('agility') ? data.agility : 0,
    wisdom: data.hasOwnProperty('wisdom') ? data.wisdom : 0,
    charm: data.hasOwnProperty('charm') ? data.charm : 0,
    luck: data.hasOwnProperty('luck') ? data.luck : 0,
  };
}

export function createStats(): Stats {
  return {
    strength: 0,
    intelligence: 0,
    agility: 0,
    wisdom: 0,
    charm: 0,
    luck: 0,
  };
}

export function createItem(): Item {
  return {
    affinity: '',
    category: '',
    constraints: '',
    flavor: '',
    gearType: '',
    icon: '',
    internalName: '',
    level: 0,
    name: '',
    rarity: 'common',
    slot: 'main_hand',
    stats: createStats(),
  };
}

export function createFighterStats(): FighterStats {
  return {
    stats: createStats(),
    level: 1,
  };
}
