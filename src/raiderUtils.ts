import { FighterBuild, FighterStats, FighterStatsEssentials, isFighterStatsEssentials, Stats } from '.';
import { isFighterStats, createFighterBuild } from './typeUtils';

/**
 * Compute the raider build stats.
 *
 * @param raiderStats
 * @returns FighterBuild
 */
export function computeRaiderBuild(raiderStats: FighterStats | FighterStatsEssentials): FighterBuild {
  if (!isFighterStats(raiderStats) && !isFighterStatsEssentials(raiderStats)) {
    throw new Error('Malformed or invalid raider stats.' + JSON.stringify(raiderStats));
  }
  let computedStats: Stats = { ...raiderStats.stats };
  if (raiderStats.mainHand) {
    const itemStats: Stats = isFighterStats(raiderStats) ? raiderStats.mainHand.stats : raiderStats.mainHand;
    computedStats.strength += itemStats.strength;
    computedStats.intelligence += itemStats.intelligence;
    computedStats.agility += itemStats.agility;
    computedStats.wisdom += itemStats.wisdom;
    computedStats.charm += itemStats.charm;
    computedStats.luck += itemStats.luck;
  }
  if (raiderStats.dress) {
    const itemStats: Stats = isFighterStats(raiderStats) ? raiderStats.dress.stats : raiderStats.dress;
    computedStats.strength += itemStats.strength;
    computedStats.intelligence += itemStats.intelligence;
    computedStats.agility += itemStats.agility;
    computedStats.wisdom += itemStats.wisdom;
    computedStats.charm += itemStats.charm;
    computedStats.luck += itemStats.luck;
  }
  if (raiderStats.finger) {
    const itemStats: Stats = isFighterStats(raiderStats) ? raiderStats.finger.stats : raiderStats.finger;
    computedStats.strength += itemStats.strength;
    computedStats.intelligence += itemStats.intelligence;
    computedStats.agility += itemStats.agility;
    computedStats.wisdom += itemStats.wisdom;
    computedStats.charm += itemStats.charm;
    computedStats.luck += itemStats.luck;
  }
  if (raiderStats.neck) {
    const itemStats: Stats = isFighterStats(raiderStats) ? raiderStats.neck.stats : raiderStats.neck;
    computedStats.strength += itemStats.strength;
    computedStats.intelligence += itemStats.intelligence;
    computedStats.agility += itemStats.agility;
    computedStats.wisdom += itemStats.wisdom;
    computedStats.charm += itemStats.charm;
    computedStats.luck += itemStats.luck;
  }
  if (raiderStats.helmet) {
    const itemStats: Stats = isFighterStats(raiderStats) ? raiderStats.helmet.stats : raiderStats.helmet;
    computedStats.strength += itemStats.strength;
    computedStats.intelligence += itemStats.intelligence;
    computedStats.agility += itemStats.agility;
    computedStats.wisdom += itemStats.wisdom;
    computedStats.charm += itemStats.charm;
    computedStats.luck += itemStats.luck;
  }

  let build = createFighterBuild();
  const strStat = { stat: 'strength', value: computedStats.strength };
  const intStat = { stat: 'intelligence', value: computedStats.intelligence };
  const agiStat = { stat: 'agility', value: computedStats.agility };
  //const wisStat = { stat: 'wisdom', value: computedStats.wisdom };
  //const chaStat = { stat: 'charm', value: computedStats.charm };
  //const lucStat = { stat: 'luck', value: computedStats.luck };
  const ratioForSortedStats = [1, 0.7, 0.05];
  //const secondaryStatsGroup = [wisStat, chaStat, lucStat];
  const primaryStatsGroupSorted = [strStat, intStat, agiStat].sort(function (e, t) {
    return t.value - e.value;
  });
  /*,
    secondaryStatsGroupSorted = secondaryStatsGroup.sort(function (e, t) {
      return t.value - e.value;
    });*/
  const strRatio = computedStats.strength * ratioForSortedStats[primaryStatsGroupSorted.indexOf(strStat)];
  const intRatio = computedStats.intelligence * ratioForSortedStats[primaryStatsGroupSorted.indexOf(intStat)];
  const agiRatio = computedStats.agility * ratioForSortedStats[primaryStatsGroupSorted.indexOf(agiStat)];
  /*const wisRatio = ratioForSortedStats[secondaryStatsGroupSorted.indexOf(wisStat)];
  const chaRatio = ratioForSortedStats[secondaryStatsGroupSorted.indexOf(chaStat)];
  const luckRatio = ratioForSortedStats[secondaryStatsGroupSorted.indexOf(lucStat)]; Not used  but i keep it just in case*/

  build.maxHP += 2.9 * strRatio + 1.5 * intRatio + 2.1 * agiRatio + 3.5 * computedStats.charm;
  build.minDamage += 0.35 * strRatio + 0.45 * intRatio + 0.4 * agiRatio + 0.5 * computedStats.wisdom;
  build.maxDamage += 0.45 * strRatio + 0.65 * intRatio + 0.55 * agiRatio + 0.55 * computedStats.wisdom;
  build.hitChance +=
    Math.pow(Math.tanh((2 * intRatio + 2 * computedStats.wisdom) / (100 + raiderStats.level)), 2) *
    (35 + Math.sqrt(3 * raiderStats.level));
  build.hitFirst +=
    Math.pow(Math.tanh((4 * agiRatio) / (100 + raiderStats.level)), 2) * (40 + Math.sqrt(3 * raiderStats.level));
  build.critDamageMultiplier +=
    Math.pow(Math.tanh((2.5 * intRatio + 2.5 * computedStats.luck) / (100 + raiderStats.level)), 2) *
      (0.4 + Math.sqrt(3 * raiderStats.level) / 100) +
    Math.tanh((0.5 * intRatio + 4 * computedStats.luck) / (100 + raiderStats.level)) *
      (0.1 + Math.sqrt(3 * raiderStats.level) / 100);
  build.meleeCrit +=
    Math.pow(Math.tanh((1 * intRatio + 2 * agiRatio + 4 * computedStats.luck) / (100 + raiderStats.level)), 2) *
      (25 + Math.sqrt(3 * raiderStats.level)) +
    Math.tanh((4 * computedStats.luck) / (100 + raiderStats.level)) * (10 + Math.sqrt(3 * raiderStats.level));
  build.critResist +=
    Math.pow(Math.tanh((2 * strRatio + 1 * computedStats.charm) / (100 + raiderStats.level)), 2) *
      (40 + Math.sqrt(3 * raiderStats.level)) +
    Math.tanh((0.5 * strRatio + 6 * computedStats.charm) / (100 + raiderStats.level)) *
      (10 + Math.sqrt(3 * raiderStats.level));
  build.evadeChance +=
    40 * Math.pow(Math.tanh((3 * agiRatio + 2 * computedStats.luck) / (100 + raiderStats.level)), 2) +
    10 * Math.tanh((0.5 * agiRatio + 6 * computedStats.luck) / (100 + raiderStats.level)) +
    Math.sqrt(3 * raiderStats.level);
  build.meleeResist +=
    Math.pow(Math.tanh((2 * strRatio + 3 * computedStats.charm) / (100 + raiderStats.level)), 2) *
    (33 + Math.sqrt(3 * raiderStats.level));

  return build;
}

export function getFighterBuildNameCap(fighterBuildName: keyof FighterBuild, level: number): number | false {
  const moke: FighterBuild = createFighterBuild();
  switch (fighterBuildName) {
    case 'hitChance':
      return moke[fighterBuildName] + 35 + Math.sqrt(3 * level);
    case 'hitFirst':
      return moke[fighterBuildName] + 40 + Math.sqrt(3 * level);
    case 'critDamageMultiplier':
      return moke[fighterBuildName] + 0.5 + (Math.sqrt(3 * level) / 100) * 2;
    case 'meleeCrit':
      return moke[fighterBuildName] + 35 + Math.sqrt(3 * level) * 2;
    case 'critResist':
      return moke[fighterBuildName] + 50 + Math.sqrt(3 * level) * 2;
    case 'evadeChance':
      return moke[fighterBuildName] + 50 + Math.sqrt(3 * level);
    case 'meleeResist':
      return moke[fighterBuildName] + 33 + Math.sqrt(3 * level);
  }
  return false;
}
