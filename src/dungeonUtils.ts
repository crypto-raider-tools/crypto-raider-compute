const dungeonXPList: Record<string, { win: number; lose: number }> = {
  MementoMori: { win: 100, lose: 0 },
  GlimmerofHope: { win: 200, lose: 0 },
  HoggersRevengeNormal: { win: 333, lose: 0 },
  HoggersRevengeHeroic: { win: 499.5, lose: 200 },
  TheChildrensCriesNormal: { win: 525, lose: 0 },
  TheChildrensCriesHeroic: { win: 650, lose: 0 },
  GuardiansLairNormal: { win: 700, lose: 0 },
  GuardiansLairHeroic: { win: 900, lose: 200 },
  OlgosDemiseNormal: { win: 800, lose: 0 },
  OlgosDemiseHeroic: { win: 1200, lose: 200 },
};

export const isDungeonName: (dungeonName: string) => boolean = (dungeonName) => {
  return Object.keys(dungeonXPList).indexOf(dungeonName) !== -1;
};

export const getDungeonXP: (dungeonName: string) => false | { win: number; lose: number } = (dungeonName) => {
  dungeonName = escapeDungeonName(dungeonName);
  if (!isDungeonName(dungeonName)) {
    return false;
  }
  return dungeonXPList[dungeonName];
};

export const escapeDungeonName: (dungeonName: string) => string = (dungeonName) => {
  return dungeonName.replace(/[^a-zA-Z0-9]/g, '');
};
