import { MobBuildName } from '.';
export * from './metaScore';
export * from './raiderUtils';
export * from './mobUtils';
export * from './typeUtils';
export * from './dungeonUtils';

export type KnickknackName = 'Bash' | 'Blind' | 'Escape Artist' | 'Ice Barrier' | 'Stone Skin' | 'Enflame' | 'Thorns';

export type FighterStats = {
  stats: Stats;
  level: number;
  mainHand?: Item;
  dress?: Item;
  knickknack?: Item;
  finger?: Item;
  neck?: Item;
  helmet?: Item;
};

export type FighterStatsEssentials = {
  stats: Stats;
  level: number;
  mainHand?: Stats;
  dress?: Stats;
  knickknack?: {
    name: KnickknackName;
  };
  finger?: Stats;
  helmet?: Stats;
  neck?: Stats;
};

export type FighterBuild = {
  maxHP: number;
  minDamage: number;
  maxDamage: number;
  hitChance: number;
  hitFirst: number;
  meleeCrit: number;
  critDamageMultiplier: number;
  critResist: number;
  evadeChance: number;
  meleeResist: number;
};

/*@deprecated*/
export type StuffStats = {
  Strength: number;
  Intelligence: number;
  Agility: number;
  Wisdom: number;
  Charm: number;
  Luck: number;
};

export type Stats = {
  strength: number;
  intelligence: number;
  agility: number;
  wisdom: number;
  charm: number;
  luck: number;
};

export type Item = {
  affinity?: string;
  category: string;
  constraints: string;
  flavor: string;
  gearType?: string;
  icon?: string;
  internalName: string;
  level?: number;
  name: string;
  rarity: Rarity;
  slot: Slot;
  stats: Stats;
};

export type Runes = 'Ice Barrier' | 'Bash' | 'Stone Skin' | 'Escape Artist' | 'Blind' | 'Enflame' | 'Thorns';
export type MetaOutput = 'OP' | 'Meta' | 'Good' | 'Decent' | 'Average' | 'Moderate' | 'Inferior' | 'Trash' | 'Ruygye';
export type Mobs = '' | MobBuildName;
export type Race = 'human' | 'white_elf' | 'blue_elf' | 'orc' | 'fairy' | 'skeleton' | 'cyborg' | 'dark_elf';
export type Rarity = 'common' | 'uncommon' | 'rare' | 'epic' | 'legendary' | 'mythic' | 'heirloom' | 'limited';
export type Slot = 'main_hand' | 'dress' | 'knickknack' | 'finger' | 'background' | 'neck' | 'helmet';
export type ItemCategory = 'gear' | 'knickknack' | 'cosmetic';
export type BuildType = 'none' | 'warrior' | 'paladin' | 'barbarian' | 'wizard' | 'rogue' | 'druid';

export type InventoryItem = {
  equipped: boolean | undefined;
  item: Item;
};

export type Avatar = {
  body: string;
  face: string;
  hair: string;
};

export type ItemMinMaxStats = {
  id: number;
  name: string;
  slot: Slot;
  count: number;
  strengthMax: number;
  strengthMin: number;
  intelligenceMax: number;
  intelligenceMin: number;
  agilityMax: number;
  agilityMin: number;
  wisdomMax: number;
  wisdomMin: number;
  charmMax: number;
  charmMin: number;
  luckMax: number;
  luckMin: number;
};

export type AbilityName =
  | 'battle_cry'
  | 'brace'
  | 'counterattack'
  | 'enchant'
  | 'magic_armor'
  | 'magic_missiles'
  | 'sharp_eyes'
  | 'sneak'
  | 'draining_strike';

export type Ability = {
  ability: AbilityName;
  equipped: boolean;
};

export type Abilities = Array<Ability>;

export type RaiderApiData = {
  avatar: Avatar;
  createdAt: string;
  generation: number;
  id: string;
  inventory: Array<InventoryItem>;
  lastRaided: string;
  level: number;
  name: string;
  new: boolean;
  paidResets: number;
  preOrderTokenId: string;
  preOrderTokenName: string;
  race: Race;
  raidsCompleted: number;
  raidsRemaining: 0;
  stats: Stats;
  tokenId: number;
  updatedAt: string;
  xp: number;
  abilities: Abilities;
};

export type RaidersApiData = {
  raiders: Array<RaiderApiData>;
};

export type BuildResult = {
  metaScore: number;
  currentClass: BuildType;
  classes: Record<BuildType, number>;
};
