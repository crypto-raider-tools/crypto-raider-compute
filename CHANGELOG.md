# CHANGELOG

<!--- next entry here -->

## 2.8.0
2022-10-09

### Features

- Added helmet (8ed1c5124199a9291f8d9dacc52fb155c6ca59b8)

## 2.7.0

2022-04-09

### Features

- Added ability types (325794c4f1ae4d57ce3da3032518f8437f90de70)

## 2.6.3

2022-03-31

### Fixes

- Update combat stats cap (d91d85f8a7696e927703d729596cd40c6288dbb9)

## 2.6.2

2022-03-23

### Fixes

- Typo, slot is neck, not necklace (45b9c5520d0853e726dc4d5489222860be44424f)

## 2.6.1

2022-03-23

### Fixes

- Missing necklace slot (281a13e1ed96250fa84933b6e93a50dd3e69741b)

## 2.6.0

2022-03-18

### Features

- Add quest web actions (0af606b090c30758b460308e8e9c00961de64d5b)

## 2.5.1

2022-03-04

### Fixes

- Update combat stats (a4170cfdc15ab4f6b5a455a2c1edc7934f9ec72e)

## 2.5.1

2022-03-04

## 2.5.0

2022-02-23

### Features

- Added finger stuff (585d238ea911f293ce19cb136932fa8ac62a1cfc)

## 2.4.0

2022-02-23

### Features

- add item category + background in slot (ea194994d1296cdf2eb3cd6abfb6fa95c440b9a1)
- update item type (29d124557408fb7723a8595eb92509b560950d5e)
- add slot for finger (daaa210a921e7a2e0d39c45f13beee958fc12ced)
- change fightStats (8a61b447f38eb912a7f60ac9bcf7be7f27d3e780)

## 2.3.0

2022-02-16

### Features

- Added robber mob (51a616f31a8658e80c4c13c874d268a90be45106)

## 2.2.2

2022-02-06

### Fixes

- Update combat stats (25f36dc8bb58f28fcd805bf1d30816b4ae77247d)

## 2.2.1

2022-02-02

### Fixes

- Updated combat stat computation (553e245ed60bf29e5041106f92d32a096282da80)

## 2.2.0

2022-02-01

### Features

- Adding dungeon xp (0dcf69be3cc21af0d70e92a5ab14d469688bec04)

## 2.1.1

2022-01-26

### Fixes

- Delivery process is now less error-prone (178a847695bec4bdaad43dd1e466b500d485cd0e)
- go-semrel-gitlab does not have yarn (9c7230f90f834d5130f1e46eae929582412969f4)
- Only the first job should be manual (ff86fa2ca9271e62d441693905ca75f22d56f93e)
