[[_TOC_]]

## How to release

- Each push on main will trigger a manual pipeline (including merge)
- This pipeline `version` will create a tag and a release
- Each tag will trigger a new pipeline
- This pipeline will build and deploy to the chrome store

Pipeline list: https://gitlab.com/crypto-raider-tools/crypto-raider-compute/-/pipelines  
Release list: https://gitlab.com/crypto-raider-tools/crypto-raider-compute/-/releases
