import { AlchemyWeb3, createAlchemyWeb3 } from '@alch/alchemy-web3';
import { AbiItem } from 'web3-utils';
import { GRIMWEED_QUEST, NEWT_QUEST, Questable } from './contract/Questable';
import { Raiders, RAIDER_CHARACTERS } from './contract/Raiders';
import { QuestingRaiders, QUESTING_RAIDERS } from './contract/QuestingRaiders';
import { BOOSTED, Boosted } from './contract/Boosted';

export const getWeb3 = (url: string): AlchemyWeb3 => {
  return createAlchemyWeb3(url);
};

type UseToastOptions = {
  title: string;
  description: string;
  status: string;
  duration: number;
  position: 'top';
  isClosable: boolean;
};

export const getAccount = async (web3: AlchemyWeb3) => {
  const accounts = await web3.eth.getAccounts();
  return accounts[0];
};

const getQuestingContract = (web3: AlchemyWeb3) =>
  new web3.eth.Contract(QuestingRaiders as AbiItem[], QUESTING_RAIDERS);

const getRaiderCharContract = (web3: AlchemyWeb3) => new web3.eth.Contract(Raiders as AbiItem[], RAIDER_CHARACTERS);

const getBoostedContract = (web3: AlchemyWeb3) => new web3.eth.Contract(Boosted as AbiItem[], BOOSTED);

const getGrimweedQuestContract = (web3: AlchemyWeb3) =>
  new web3.eth.Contract(Questable as AbiItem[], process.env.NEXT_PUBLIC_GRIMWEED_QUEST);

const getNewtQuestContract = (web3: AlchemyWeb3) =>
  new web3.eth.Contract(Questable as AbiItem[], process.env.NEXT_PUBLIC_NEWT_QUEST);

export async function areRaidersApproved(quest: string, web3: AlchemyWeb3) {
  const account = await getAccount(web3);
  try {
    const result = await getRaiderCharContract(web3).methods.isApprovedForAll(account, quest).call();
    return result;
  } catch (err) {
    console.error(err);
  }
}

export async function getRaiderSpeedBoost(raiderId: number, web3: AlchemyWeb3) {
  try {
    const result = await getBoostedContract(web3).methods.raiderSpeedBoost(raiderId).call();
    return result;
  } catch (err) {
    console.error(err);
  }
}

const questResolver = (quest: string, web3: AlchemyWeb3) => {
  if (quest === 'GRIMWEED') {
    return getGrimweedQuestContract(web3);
  } else if (quest === 'NEWT') {
    return getNewtQuestContract(web3);
  } else {
    throw new Error(`${quest} invalid quest`);
  }
};

export async function startQuest(
  raiderId: number,
  questString: string,
  callback: (options: UseToastOptions) => string | number | undefined,
  web3: AlchemyWeb3
) {
  const quest = questResolver(questString, web3);
  try {
    const account = await getAccount(web3);
    const result = await quest.methods
      .startQuest(raiderId)
      .send({ from: account, maxPriorityFeePerGas: null, maxFeePerGas: null }, function (err: any, res: any) {
        if (err) {
          callback({
            title: "Couldn't start your quest...",
            description: err.message,
            status: 'error',
            duration: 9000,
            position: 'top',
            isClosable: true,
          });
          console.log(err);
        }
        console.log('Hash of the transaction: ' + res);
      });
    return result;
  } catch (err) {
    console.error(err);
  }
}

export async function endQuest(
  raiderId: number,
  questString: string,
  toast: (options: UseToastOptions) => string | number | undefined,
  web3: AlchemyWeb3
) {
  const quest = questResolver(questString, web3);
  try {
    const account = await getAccount(web3);
    const result = await quest.methods
      .endQuest(raiderId)
      .send({ from: account, maxPriorityFeePerGas: null, maxFeePerGas: null }, function (err: any, res: any) {
        if (err) {
          toast({
            title: "Couldn't end your quest...",
            description: err.message,
            status: 'error',
            duration: 9000,
            position: 'top',
            isClosable: true,
          });
          console.log(err);
        }
        console.log('Hash of the transaction: ' + res);
      });
    return result;
  } catch (err) {
    console.error(err);
  }
}

export async function runHome(
  raiderId: number,
  questString: string,
  toast: (options: UseToastOptions) => string | number | undefined,
  web3: AlchemyWeb3
) {
  const quest = questResolver(questString, web3);
  try {
    const account = await getAccount(web3);
    const result = await quest.methods
      .runHome(raiderId)
      .send({ from: account, maxPriorityFeePerGas: null, maxFeePerGas: null }, function (err: any, res: any) {
        if (err) {
          toast({
            title: "Couldn't run home...",
            description: err.message,
            status: 'error',
            duration: 9000,
            position: 'top',
            isClosable: true,
          });
          console.log(err);
        }
        console.log('Hash of the transaction: ' + res);
      });
    return result;
  } catch (err) {
    console.error(err);
  }
}

export async function getRewards(
  raiderId: number,
  questString: string,
  toast: (options: UseToastOptions) => string | number | undefined,
  web3: AlchemyWeb3
) {
  const quest = questResolver(questString, web3);
  try {
    const account = await getAccount(web3);
    const result = await quest.methods
      .getRewards(raiderId)
      .send({ from: account, maxPriorityFeePerGas: null, maxFeePerGas: null }, function (err: any, res: any) {
        if (err) {
          toast({
            title: "Couldn't get rewards...",
            description: err.message,
            status: 'error',
            duration: 9000,
            position: 'top',
            isClosable: true,
          });
          console.log(err);
        }
        console.log('Hash of the transaction: ' + res);
      });
    return result;
  } catch (err) {
    console.error(err);
  }
}

export async function getQuestStatus(raider: number, questString: string, web3: AlchemyWeb3) {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.raiderStatus(raider).call();
    return result;
  } catch (err) {
    console.error(err);
  }
}

export async function getTimeTillHome(
  raider: number,
  questString: string,
  web3: AlchemyWeb3
): Promise<number | undefined> {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.timeTillHome(raider).call();
    return Number(result);
  } catch (err) {
    console.error(err);
  }
  return;
}

export async function getTotalReturnTime(
  raider: number,
  questString: string,
  web3: AlchemyWeb3
): Promise<number | undefined> {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.calcReturnTime(raider).call();
    return Number(result);
  } catch (err) {
    console.error(err);
  }
  return;
}

export async function getQuestDuration(
  raider: number,
  questString: string,
  web3: AlchemyWeb3
): Promise<number | undefined> {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.timeQuesting(raider).call();
    return Number(result);
  } catch (err) {
    console.error(err);
  }
  return;
}

export async function getEarned(raider: number, questString: string, web3: AlchemyWeb3): Promise<number | undefined> {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.calcEarned(raider).call();
    return Number(result);
  } catch (err) {
    console.error(err);
  }
  return;
}

export async function getRewardsToCollect(
  raider: number,
  questString: string,
  web3: AlchemyWeb3
): Promise<number | undefined> {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.rewardEarned(raider).call();
    return Number(result);
  } catch (err) {
    console.error(err);
  }
  return;
}

export async function nextReward(raider: number, questString: string, web3: AlchemyWeb3): Promise<number | undefined> {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.nextReward(raider).call();
    return Number(result);
  } catch (err) {
    console.error(err);
  }
  return;
}

export async function rewardTime(raider: number, questString: string, web3: AlchemyWeb3): Promise<number | undefined> {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.calcRaiderRewardTime(raider).call();
    return Number(result);
  } catch (err) {
    console.error(err);
  }
  return;
}

export async function minExpForQuest(questString: string, web3: AlchemyWeb3): Promise<number | undefined> {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.minProfessionExp().call();
    return Number(result);
  } catch (err) {
    console.error(err);
  }
  return;
}

export async function expRewardAmount(questString: string, web3: AlchemyWeb3): Promise<number | undefined> {
  const quest = questResolver(questString, web3);
  try {
    const result = await quest.methods.experienceAmount().call();
    return Number(result);
  } catch (err) {
    console.error(err);
  }
  return;
}

const questFinder = (contract: string): 'GRIMWEED' | 'NEWT' | undefined => {
  if (contract === GRIMWEED_QUEST) {
    return 'GRIMWEED';
  } else if (contract === NEWT_QUEST) {
    return 'NEWT';
  }
  return;
};

export async function getRaiderQuest(raider: number, web3: AlchemyWeb3): Promise<'GRIMWEED' | 'NEWT' | undefined> {
  try {
    const result = await getQuestingContract(web3).methods.raiderQuest(raider).call();
    return questFinder(result);
  } catch (err) {
    console.error(err);
  }
  return;
}
